#include "../headers/game.h"
#include <sstream>


int main (int argc, char** argv)
{
	Game *game = new Game();

	game->start();
    while (!game->isDone())
    {
		game->update();
    }

    printf("Exited cleanly\n");
    return 0;
}
