#include "../headers/timer.h"

Timer::Timer() {
    startTicks = 0;
    pausedTicks = 0;
	timeDiff = 0;
    paused = false;
    started = false;
}

void Timer::start() {
    started = true;
    paused = false;

    startTicks = SDL_GetTicks();
	timeDiff = startTicks;
}

void Timer::stop() {
    started = false;
    paused = false;
}

void Timer::pause() {
    if((started == true) && (paused == false)) {
        paused = true;
        pausedTicks = SDL_GetTicks() - startTicks;
    }
}

void Timer::unpause() {
    if(paused == true) {
        startTicks = SDL_GetTicks() - pausedTicks;
        paused = false;
        pausedTicks = 0;
    }
}

int Timer::getTicks() {
    if(started == true) {
        if(paused == true) {
            return pausedTicks;
        } else {
            return SDL_GetTicks() - startTicks;
        }
    }

    return 0;
}

int Timer::getDiff() {
	if (started) {
		if (!paused) {
			Uint32 oldTime = timeDiff;
			timeDiff = SDL_GetTicks();
			return timeDiff - oldTime; // miliseconds
		}
	}

	return 0;
}

bool Timer::isStarted() {
    return started;
}

bool Timer::isPaused() {
    return paused;
}
