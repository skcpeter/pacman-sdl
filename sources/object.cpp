#include "../headers/object.h"


Object::Object(int x, int y) {
	offset = new SDL_Rect;
	offset->x = x*TILE_SIZE;
	offset->y = y*TILE_SIZE;

	load();
}

void Object::draw(SDL_Surface *screen) {
	// offset -> miejsce obrazka na ekranie (x, y)
	// clip -> rozmiar obrazka wraz z przesunieciem, width, height, x, y ze sprite

	SDL_BlitSurface(sprites, getCurrentFrame(), screen, offset);
}

void Object::update(unsigned time) {
	return;
}

void Object::load() {
	//The image that's loaded
	SDL_Surface* loadedImage = NULL;

	//Load the image
	loadedImage = IMG_Load("resources/spritesheet.png");

	//If the image loaded
	if (loadedImage != NULL) {
		//Create an optimized surface
		sprites = SDL_DisplayFormat(loadedImage);

		//Free the old surface
		SDL_FreeSurface(loadedImage);

		//If the surface was optimized
		if (sprites != NULL) {
			//Color key surface
			SDL_SetColorKey(sprites, SDL_SRCCOLORKEY, SDL_MapRGB(sprites->format, 128, 0, 128));
		}
		else {
			cout << "Invalid optimize image: " << SDL_GetError() << endl;
		}
	}
	else {
		cout << "Invalid load sprite load: " << SDL_GetError() << endl;
	}
}

void Object::destroy() {
	
}





Wall::Wall(int x, int y) : Object(x, y) {

	image = new SDL_Rect;
	image->x = 270;
	image->y = 90;
	image->w = image->h = 27;
}

SDL_Rect *Wall::getCurrentFrame() {
	return image;
}

Point::Point(int x, int y) : Object(x, y) {
	image = new SDL_Rect;
	image->x = 240;
	image->y = 90;
	image->w = image->h = 27;
}

SDL_Rect *Point::getCurrentFrame() {
	return image;
}





Player::Player(int x, int y) : Object(x, y) {
	maxFrame = 3;
	frame = 0;
	timer = 0;
	route = 0;
	nextRoute = 0;
	routeTimer = 0;
	frameMultipler = 1;
	canNextRoute = false;

	canMove = true;
	collision = false;
	bindAnimation();
}

void Player::bindAnimation() {
	int frame = 0;
	for (int route = 0; route < 4; route++) {
		for (int animate = 0; animate < maxFrame; animate++) {
			animates[route][animate] = new SDL_Rect();
			if (animate == 0) {
				animates[route][animate]->x = 0;
			}
			else {
				frame++;
				animates[route][animate]->x = 30 * frame;
			}

			animates[route][animate]->y = 0;
			animates[route][animate]->w = 27;
			animates[route][animate]->h = 27;
		}
	}
}

SDL_Rect *Player::getCurrentFrame() {
	if (!canMove)
		return animates[0][0];

	return animates[route][frame]; 
}

void Player::update(int diff) {
	if (collision)
		return;

	/* 1000 / 25 (fps) = 40, 40 * frameCap (3) = 120 */
	if (timer >= 120) {
		timer = 0;
		changeFrame();
	} 
	else {
		timer += diff;
	}

	if (routeTimer > 500) {
		routeTimer = 0;
		nextRoute = route;
	}
	else {
		routeTimer += diff;
	}

	updatePosition(diff);
}

void Player::changeFrame() {
	if (frame == 2) {
		frameMultipler = -1;
	}
	else if (frame == 0) {
		frameMultipler = 1;
	}
	
	frame += frameMultipler;
}

int Player::getNextPosition(int diff) {
	if (posX > 1) {
		posX -= 1;
		return 1;
	}

	posX += 100 * diff / 1000.0f;
	return 0;
}

void Player::updatePosition(int diff) {
	int distance = getNextPosition(diff);
	
	switch (route) {
		case LEFT:
			offset->x -= distance;
			break;
		case RIGHT:
			offset->x += distance;
			break;
		case UP:
			offset->y -= distance;
			break;
		case DOWN:
			offset->y += distance;
			break;
	}
}

void Player::setNextRoute(int route) {
	nextRoute = route;
	canMove = true;
}

bool Player::hasCollisionWithWall(Object *object) {
	int oldRoute = route;
	bool tmpCollision;
	route = nextRoute;
	potentialPosition();
	if (hasCollision(object)) {
		canNextRoute = false;
	}
	rollbackPosition();

	route = oldRoute;

	potentialPosition();
	tmpCollision = hasCollision(object);
	if (!collision) {
		collision = tmpCollision;
	}
	rollbackPosition();
	return tmpCollision;
}

void Player::beginCollision() {
	canNextRoute = true;
	collision = false;
}

void Player::endCollision() {
	if (canNextRoute) {
		route = nextRoute;
	}
}

bool Player::hasCollision(Object *object) {
	int left1, left2;
	int right1, right2;
	int top1, top2;
	int bottom1, bottom2;

	left1 = offset->x;
	left2 = object->getOffset()->x;
	right1 = offset->x + offset->w;
	right2 = object->getOffset()->x + object->getOffset()->w;
	top1 = offset->y;
	top2 = object->getOffset()->y;
	bottom1 = offset->y + offset->h;
	bottom2 = object->getOffset()->y + object->getOffset()->h;

	if (bottom1 <= top2) return false;
	if (top1 >= bottom2) return false;

	if (right1 <= left2) return false;
	if (left1 >= right2) return false;

	return true;
}

void Player::potentialPosition() {
	switch (route) {
		case LEFT:
			offset->x -= 1;
			break;
		case RIGHT:
			offset->x += 1;
			break;
		case UP:
			offset->y -= 1;
			break;
		case DOWN:
			offset->y += 1;
			break;
	}
}

void Player::rollbackPosition() {
	switch (route) {
		case LEFT:
			offset->x += 1;
			break;
		case RIGHT:
			offset->x -= 1;
			break;
		case UP:
			offset->y += 1;
			break;
		case DOWN:
			offset->y -= 1;
			break;
	}
}




Ghost::Ghost(int x, int y, char type) : Object(x, y), type(type) {
	maxFrame = 2;
	route = 0;
	frame = 0;
	bindAnimation();
}

SDL_Rect *Ghost::getCurrentFrame() {
	return animates[route][frame];
}

void Ghost::bindAnimation() {
	int frame = 0;
	int gridX = 0;
	int gridY = 30;

	switch (type) {
		case '2':
			gridX = 8 * 30;
			break;
		case '3':
			gridY = 60;
			break;
		case '4':
			gridX = 8 * 30;
			gridY = 60;
			break;
	}

	for (int route = 0; route < 4; route++) {
		for (int animate = 0; animate < maxFrame; animate++) {
			animates[route][animate] = new SDL_Rect();
			animates[route][animate]->x = gridX + 30 * frame;
			animates[route][animate]->y = gridY;
			animates[route][animate]->w = 27;
			animates[route][animate]->h = 27;

			frame++;
		}
	}
}

void Ghost::update(int diff) {
	/* 1000 / 25 (fps) = 40, 40 * frameCap (3) = 120 */
	if (timer >= 120) {
		timer = 0;
		changeFrame();
	}
	else {
		timer += diff;
	}

	updatePosition(diff);
}

void Ghost::changeFrame() {
	if (frame == 2) {
		frameMultipler = -1;
	}
	else if (frame == 0) {
		frameMultipler = 1;
	}

	frame += frameMultipler;
}

int Ghost::getNextPosition(int diff) {
	if (posX > 1) {
		posX -= 1;
		return 1;
	}

	posX += 100 * diff / 1000.0f;
	return 0;
}

void Ghost::updatePosition(int diff) {
	if (!canMove)
		return;

	int distance = getNextPosition(diff);

	switch (route) {
	case LEFT:
		offset->x -= distance;
		break;
	case RIGHT:
		offset->x += distance;
		break;
	case UP:
		offset->y -= distance;
		break;
	case DOWN:
		offset->y += distance;
		break;
	}
}

void Ghost::setNextRoute(int route) {
	
	canMove = true;
}

bool Ghost::hasCollisionWithWall(Object *object) {
	bool collision;

	potentialPosition();
	collision = hasCollision(object);
	rollbackPosition();
	return collision;
}

void Ghost::beginCollision() {
	
}

void Ghost::endCollision() {

}

bool Ghost::hasCollision(Object *object) {
	int left1, left2;
	int right1, right2;
	int top1, top2;
	int bottom1, bottom2;

	left1 = offset->x;
	left2 = object->getOffset()->x;
	right1 = offset->x + offset->w;
	right2 = object->getOffset()->x + object->getOffset()->w;
	top1 = offset->y;
	top2 = object->getOffset()->y;
	bottom1 = offset->y + offset->h;
	bottom2 = object->getOffset()->y + object->getOffset()->h;

	if (bottom1 <= top2) return false;
	if (top1 >= bottom2) return false;

	if (right1 <= left2) return false;
	if (left1 >= right2) return false;

	return true;
}

void Ghost::potentialPosition() {
	switch (route) {
	case LEFT:
		offset->x -= 1;
		break;
	case RIGHT:
		offset->x += 1;
		break;
	case UP:
		offset->y -= 1;
		break;
	case DOWN:
		offset->y += 1;
		break;
	}
}

void Ghost::rollbackPosition() {
	switch (route) {
	case LEFT:
		offset->x += 1;
		break;
	case RIGHT:
		offset->x -= 1;
		break;
	case UP:
		offset->y += 1;
		break;
	case DOWN:
		offset->y -= 1;
		break;
	}
}
