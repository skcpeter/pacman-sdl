#include "../headers/game.h"


Game::Game() {
	map = new Map();
	timer = new Timer();
	done = false;

	initialize();
}

void Game::initialize() {

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("Unable to init SDL: %s\n", SDL_GetError());
        exit(1);
    }

	SDL_WM_SetCaption( "Animation Test", NULL );

    // make sure SDL cleans up before exit
    atexit(SDL_Quit);

    // create a new window

	if (TTF_Init() == -1) {
		return;
	}
	
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE);
    if (!screen) {
        printf("Unable to set 640x480 video: %s\n", SDL_GetError());
        exit(1);
    }


	map->loadMap("resources/map/map");
}

void Game::update() {
	handleInput();


	int diff = timer->getDiff();
	if (diff > 0)
		draw(diff);
}

void Game::draw(int diff) {
	SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));

	map->update(diff);
	map->draw(screen);

	SDL_Flip(screen);
}


void Game::handleInput() {
	Player *player = map->getPlayer();

	SDL_Event event;
	while (SDL_PollEvent(&event))
	{

		// exit if the window is closed
		if (event.type == SDL_QUIT) {
			done = true;
		}
		else if (event.type == SDL_KEYDOWN) {
			switch (event.key.keysym.sym) {
				case SDLK_LEFT:
					player->setNextRoute(LEFT);
					break;
				case SDLK_RIGHT:
					player->setNextRoute(RIGHT);
					break;
				case SDLK_UP:
					player->setNextRoute(UP);
					break;
				case SDLK_DOWN:
					player->setNextRoute(DOWN);
					break;

				case SDLK_ESCAPE:
					stop();
					break;
				case SDLK_s:
					if (timer->isStarted())
					{

						timer->stop();
					}
					else
					{

						timer->start();
					}
					break;
				case SDLK_p:
					//If the timer is paused
					if (timer->isPaused())
					{
						//Unpause the timer
						timer->unpause();
					}
					else
					{
						//Pause the timer
						timer->pause();
					}
					break;
			}
		}
	} 
}

bool Game::isDone() {
	return done;
}

void Game::start() {
	done = false;
	timer->start();
}

void Game::stop() {
	timer->stop();
	done = true;
}