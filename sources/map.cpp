#include "../headers/map.h"

Map::Map() {}

void Map::loadMap(string filename) {
	
	ifstream file(filename);
    string line; 
	int y = 0;
	if(file.is_open()){ 
		while (getline(file, line)) {
			for (size_t i = 0; i < line.size(); i++) {
				push(i, y, line[i]);
			}
			y++; 
		}
		file.close();
	} else {
		cout << "Unable to open file " << filename << endl;
	}
}

void Map::push(int x, int y, char type) {
	switch(type) {
		case '.': // wall
			blocks.push_back(new Wall(x, y));
			break;
		case 'o': // point
			points.push_back(new Point(x, y));
			break;
		case 'P':
			player = new Player(x, y);
			break;
		case '1':
		case '2':
		case '3':
		case '4':
			ghosts.push_back(new Ghost(x, y, type));
			break;
			
	}
}

void Map::draw(SDL_Surface *screen) {
	for (size_t i = 0; i < blocks.size(); i++) {
		blocks[i]->draw(screen);
	}
	for (size_t i = 0; i < points.size(); i++) {
		points[i]->draw(screen);
	}
	for (size_t i = 0; i < ghosts.size(); i++) {
		ghosts[i]->draw(screen);
	}

	player->draw(screen);
}

void Map::update(int diff) {
	hasCollision();

	player->update(diff);
}

bool Map::hasCollision() {
	bool collision = false;
	player->beginCollision();
	for (size_t i = 0; i < blocks.size(); i++) {
		if (player->hasCollisionWithWall(blocks[i]))
			collision = true;
	}

	for (size_t i = 0; i < points.size(); i++) {
		if (player->hasCollision(points[i])) {
			delete points[i];
			points.erase(points.begin()+i);
		}
	}

	player->endCollision();

	return collision;
}