#pragma once


const int RIGHT = 0;
const int DOWN = 1;
const int LEFT = 2;
const int UP = 3;

const int MAP_HEIGHT = 15;
const int MAP_WIDTH = 15;
const int TILE_SIZE = 27;

const int FRAME_CAP = 25;

const int SCREEN_WIDTH = MAP_WIDTH * TILE_SIZE;
const int SCREEN_HEIGHT = MAP_HEIGHT * TILE_SIZE;
const int SCREEN_BPP = 32;