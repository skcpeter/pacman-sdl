#pragma once

#include "libs.h"
#include "map.h"
#include "timer.h"

class Game {
    private:
		SDL_Surface *screen;
		Timer *timer;
		Map *map;
		bool done;


		void stop();
		void draw(int diff);
		void initialize();
		void handleInput();
    public:
		Game();

		void start();
		void update();
		bool isDone();
};