#pragma once

#ifdef __APPLE__
	#include <SDL/SDL.h>
	#include <SDL/SDL_image.h>
	#include <SDL/SDL_ttf.h>
#else
	#include <SDL.h>
	#include <SDL_image.h>
	#include <SDL_ttf.h>
#endif

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "config.h"

using namespace std;