#pragma once

#include "libs.h"
#include "object.h"

class Map {

	private:
		vector<Point*> points;
		vector<Wall*> blocks;
		vector<Ghost*> ghosts;
		Player *player;

		void push(int x, int y, char type);
		bool hasCollision();
	public:
		Map();

		void loadMap(string filename);
		void draw(SDL_Surface *screen);
		void update(int diff);

		Player *getPlayer() { return player; }
};
