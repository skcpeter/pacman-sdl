#pragma once

#include "libs.h"

class Timer {
    private:
		Uint32 startTicks;
		Uint32 pausedTicks;
		Uint32 timeDiff;
        bool paused;
        bool started; 

    public:
        Timer();

        void start();
        void stop();
        void pause();
        void unpause();

        int getTicks();
		int getDiff();

        bool isStarted();
        bool isPaused();
};
