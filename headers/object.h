#pragma once

#include "libs.h"


class Object {

	protected:
		SDL_Surface *sprites;
		SDL_Surface *surface;
		SDL_Rect *offset;

		virtual SDL_Rect *getCurrentFrame() = 0;
	public:
		explicit Object(int x, int y);
		

		void draw(SDL_Surface *screen);
		void update(unsigned time);	
		void load();

		void destroy();

		SDL_Rect *getOffset() { return offset; }

};


class Wall : public Object {
	private:
		SDL_Rect *image;

	protected:
		SDL_Rect *getCurrentFrame();

	public:
		explicit Wall(int x, int y);
};

class Point : public Object {
	private:
		SDL_Rect *image;

	protected:
		SDL_Rect *getCurrentFrame();

	public:
		explicit Point(int x, int y);
};

class Creature {
	protected:

		int xVel, yVel;
};


/**
Zak�adamy 25 klatek na sekdunde, czyli 1000 / 25 = 40. Co 40 ms zmieniamy animacje
*/
class Player : public Object, public Creature {
	private:
		SDL_Rect *animates[4][3];
		int frameMultipler;
		int maxFrame;
		int frame;
		int timer;
		int routeTimer;

		int route;
		int nextRoute;
		bool canNextRoute;
		bool canMove;
		bool collision;
		double posX;
		double posY;

		/* animate */
		void bindAnimation();
		void changeFrame();

		/* position */
		void updatePosition(int diff);
		int getNextPosition(int diff);

		/* collision helpers */
		void potentialPosition();
		void rollbackPosition();

	protected:
		SDL_Rect *getCurrentFrame();

	public:
		Player(int x, int y);
		

		void changeRoute();
		void setNextRoute(int route);
		void update(int diff);
		
	
		void beginCollision();
		void endCollision();
		bool hasCollision(Object *object);
		bool hasCollisionWithWall(Object *object);

};

class Ghost : public Object, public Creature {
	private:
		SDL_Rect *animates[4][2];
		char type;

		int frameMultipler;
		int maxFrame;
		int frame;
		int timer;

		int route;
		bool canMove;
		double posX;
		double posY;

		/* animate */
		void bindAnimation();
		void changeFrame();

		/* position */
		void updatePosition(int diff);
		int getNextPosition(int diff);

		/* collision helpers */
		void potentialPosition();
		void rollbackPosition();

	protected:
		SDL_Rect *getCurrentFrame();

	public:
		Ghost(int x, int y, char type);

		
		void changeRoute();
		void setNextRoute(int route);
		void update(int diff);


		void beginCollision();
		void endCollision();
		bool hasCollision(Object *object);
		bool hasCollisionWithWall(Object *object);
};